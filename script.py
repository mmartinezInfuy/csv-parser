import csv
import json
blacklist = ['mL', 'g', 'gp', 'p', 'gk', 'sg', 'k', 'mg', 'mg)', 'mg/mL', 'mg/g', 'mcg', 'MUI', 'd', 'UI', 'U', 'r', '+']
with open('drugs.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0
    effective_rows = 0
    multiple_data = 0
    missing_lines = []
    drugs = []
    medicaments = []
    for row in csv_reader:
        if line_count % 2 == 0 :
            medicament_description = row[5].split()
            res = [ (index, int(i), medicament_description[index + 1]) for index, i in
                   enumerate(medicament_description) if
                       i.isdigit() and
                       not [ y for y in medicament_description[index + 1]  if
                           y.isdigit() or
                           y == '/'] and
                           not medicament_description[index + 1] in blacklist ]
            if len(res) == 1:
                found = False
                drug_id=row[0].zfill(24)
                for drug in drugs:
                    if drug['code'] == drug_id:
                        found = True
                        break
                if not found:
                    drug_name=row[2]
                    drug = {"_id": { "$oid": drug_id }, "code": drug_id, "name": drug_name }
                    drugs.append(drug)

                found = False
                medicament_id = row[3]
                for medicament in medicaments:
                    if medicament['code'] == medicament_id:
                        found = True
                        break
                if not found:
                    medicament_name = row[5]
                    medicament_drug = row[0].zfill(24)
                    medicament_qty = res[0][1]
                    medicament = { "code": medicament_id, "quantity": medicament_qty, "drug": { "$oid": medicament_drug },  "name": medicament_name }
                    medicaments.append(medicament)

                effective_rows += 1
        line_count += 1
    certain_rows = effective_rows - multiple_data
    with open('drugs.json', 'w') as filehandle:
       for index, text in enumerate(json.dumps(drugs).split("},"), start=1):
           if not index % 2 == 0:
               filehandle.write('%s},' % text)
           else:
               filehandle.write('%s}\n' % text)
    with open('medicaments.json', 'w') as filehandle:
        for index, text in enumerate(json.dumps(medicaments).split("},"), start=1):
            if not index % 2 == 0:
                filehandle.write('%s},' % text)
            else:
                filehandle.write('%s}\n' % text)
    print(f"finished reading {effective_rows} rows where {multiple_data} rows have unidentified values, we got {certain_rows} rows with certain information")
    print(missing_lines)
